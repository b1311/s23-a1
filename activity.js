db.rooms.insertOne(
		{
			"name" : "single",
			"accomodates" : 2,
			"price" : 1000,
			"description" : "A simple room with all the basic necessities",
			"rooms_available" : 10,
			"isAvailable" : false
		}
)


db.rooms.insertMany([
		{
			"name" : "double",
			"accomodates" : 3,
			"price" : 2000,
			"description" : "A room fit for a small family going on a vacation",
			"rooms_available" : 5,
			"isAvailable" : false
		},
		{
			"name" : "queen room",
			"accomodates" : 2,
			"price" : 3000,
			"description" : "A large room perfect for couple",
			"rooms_available" : 3,
			"isAvailable" : true
		}
])

db.rooms.find({"name" : "double"})

db.rooms.updateOne({"name" : "queen room"},  {$set:{"rooms_available" : 0}})

db.rooms.deleteMany({"isAvailable" : false})